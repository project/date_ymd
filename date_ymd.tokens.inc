<?php

/**
 * @file
 * Provides Token integration for the Date YMD field.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Implements hook_token_info().
 */
function date_ymd_token_info() {
  $types = [];
  $tokens = [];
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if (!$entity_type->entityClassImplements(ContentEntityInterface::class)) {
      continue;
    }
    $token_type = \Drupal::service('token.entity_mapper')->getTokenTypeForEntityType($entity_type_id);
    if (empty($token_type)) {
      continue;
    }

    // Build year-only tokens for all Date YMD fields.
    $fields = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions($entity_type_id);
    foreach ($fields as $field_name => $field) {
      if ($field->getType() != 'date_ymd') {
        continue;
      }

      $tokens[$token_type . '-' . $field_name]['year_only'] = [
        'name' => t('Year only'),
        'description' => NULL,
        'module' => 'date_ymd',
      ];
    }
  }

  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements hook_tokens().
 */
function date_ymd_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if (!empty($data['field_property'])) {
    foreach ($tokens as $token => $original) {
      $delta = 0;
      $parts = explode(':', $token);
      if (is_numeric($parts[0])) {
        if (count($parts) > 1) {
          $delta = $parts[0];
          $property_name = $parts[1];
        }
        else {
          continue;
        }
      }
      else {
        $property_name = $parts[0];
      }
      if ($property_name != 'year_only') {
        continue;
      }
      if (!isset($data[$data['field_name']][$delta])) {
        continue;
      }

      $field_item = $data[$data['field_name']][$delta];
      $replacements[$original] = substr($field_item->value, 0, 4);;
    }
  }

  return $replacements;
}
