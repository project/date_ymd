<?php

namespace Drupal\date_ymd\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;

/**
 * Plugin implementation of the 'date_ymd' field type.
 *
 * @FieldType(
 * id = "date_ymd",
 * label = @Translation("Date YMD"),
 * description = @Translation("Provides a date field which allows year, year/month and year/month/day values."),
 * default_widget = "date_ymd",
 * default_formatter = "date_ymd",
 * )
 */
class DateYMD extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'int',
          'length' => 50,
        ],
      ],
      'indexes' => [
        'value' => [
          'value',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['value'] = DataDefinition::create('integer');
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if (isset($values['date_ymd'])) {
      $values['value'] = $values['date_ymd']['year'] . $values['date_ymd']['month'] . $values['date_ymd']['day'];
    }
    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '' || $value === '00000000';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'date_ymd_begin_year' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['date_ymd_begin_year'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Beginning year'),
      '#default_value' => $this->getSetting('date_ymd_begin_year'),
      '#description' => $this->t('Enter the lowest option for the year selector.'),
      '#size' => 6,
      '#required' => TRUE,
    ];

    return $element;
  }

}
